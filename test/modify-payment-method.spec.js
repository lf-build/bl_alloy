describe("<modify-payment-method>", function() {
    var component;

    beforeEach(function(done) {
        component = container.get("modify-payment-method");
        component = Object.create(component);
        done();
    });


    it("component is defined", function() {
        chai.assert(component, "component should be defined");
    });

    describe("properties", function() {
        var expectedProperties = [
            "radioButtons",
            "alertCfg",
            "radioButtonAction",
            "alertClickAction",
            "state",
            "currentLoan"
        ];

        expectedProperties.forEach(function(prop) {
            it(prop, function() {
                chai.assert(component.properties[prop], prop + " should be defined");
            });

        })
    });

    describe("methods", function() {
        var hub;
        beforeEach(function() {
            component.dispatch = sinon.spy();
            component.getState = sinon.spy();
            component.subscribe = sinon.spy();
            hub = {
                publish: sinon.spy()
            };
        });

        function testMethodDispatching(methodName, actionType) {
            describe(methodName, function() {
                it("dispatches the correct action type", function() {
                    component[methodName]();
                    chai.assert(
                        component.dispatch.calledWith({ type: actionType }),
                        methodName + " should dispatch action with type " + actionType
                    );
                });
            });
        }

        testMethodDispatching("resetForm", "RESET_FORM");

        testMethodDispatching("cancelForm", "CANCEL_FORM");

        testMethodDispatching("setCurrentLoan", "SET_CURRENT_LOAN");

        describe("isCheck", function() {
            it("returns false with input of ach", function() {
                chai.assert.isNotOk(
                    component.isCheck("ach"),
                    "should return false"
                );
            });

            it("returns false with input of ACH", function() {
                chai.assert.isNotOk(
                    component.isCheck("ACH"),
                    "should return false"
                );
            });

            it("returns true with input of check", function() {
                chai.assert.ok(
                    component.isCheck("check"),
                    "should return true"
                );
            });

            it("returns true with input of Check", function() {
                chai.assert.ok(
                    component.isCheck("Check"),
                    "should return true"
                );
            });
        });

        describe("shouldHideBankList", function() {
            describe("with bankInfo.length !== 0", function() {
                describe("with paymentMethod as check", function() {
                    it("returns true");
                });

                describe("with paymentMethod as ach", function() {
                    it("returns false");
                });
            });

            describe("with bank info.length === 0", function() {
                it("returns true");
            });
        });

        describe("shouldHideErrorMessage", function() {
            it("returns true when message is undefined", function() {
              var value = component.shouldHideErrorMessage(undefined);
              chai.assert(value);
            });

            it("returns false when message length > 0", function() {
              var value = component.shouldHideErrorMessage("a message");
              chai.assert.isNotOk(value);
            });

            it("returns true when message length is 0", function() {
              var value = component.shouldHideErrorMessage("");
              chai.assert(value);
            });
        });
    });
});
