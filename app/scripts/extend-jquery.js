$.extend($.expr[':'], {
    inputComponent : function(e) {
        return /input$/i.test(e.nodeName);
    },
    inputComponentClass : function(e) {
        var classList = Array.prototype.slice.call(e.classList);
        for (var i = 0; i < classList.length; i++) {
            var elClass = classList[i];
            if (/input$/i.test(elClass)) {
                return true;
            }
        }
        return false;
    }
});

var oldVal = $.fn.val;
$.fn.val = function() {
    var result = oldVal.apply(this, arguments);
    if (this[0] && typeof this[0].val === "function") {
        result = this[0].val.apply(this[0], arguments);
    } 
    return result;
}