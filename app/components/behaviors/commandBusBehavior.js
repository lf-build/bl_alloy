/* jshint -W098 */
var commandBus = (function() {
    var commandUid = -1;
    // Generates proxy methods on the host object corresponding to the keys in the commands property on the host object
    function mapCommandsToMethods() {
        function createProxy(name) {
            return function proxy() {
                return this.dispatch(name, arguments);
            };
        }

        function commandNameError(name, context) {
            return [
                "The command named ",
                name,
                " is already used on element ",
                context
            ].join("");
        }

        if (this.commands) {
            for (var name in this.commands) {
                if (this.commands.hasOwnProperty(name)) {
                    if (this[name]) {
                        console.error(commandNameError(name, this));
                        return;
                    }

                    // Get a ref to the correct command handler
                    var handler = this.commands[name];

                    // Register the command within the command bus
                    this.register(name, handler);

                    // Generate the proxy method and set it on the host object (this)
                    /* jshint -W083 */
                    this.set(name, createProxy(name));
                }
            }
        }
    }

    return {
        // Component lifecycle event
        registered: function() {
            // Holds a map of command types and handlers unique to each host object
            this.set("registry", {});
            mapCommandsToMethods.call(this);
        },

        attached: function() {
            if (typeof this.subscribe !== 'function' || typeof this.publish !== 'function') {
                console.error("Could not detect publish or subscribe methods. commandBusBehavior must be used with eventHubBehavior.");
            }
        },




        // Add a command with corresponding handler to the registry
        register: function(name, handler) {

            var alreadyRegisteredError = [
                "Command named ",
                name,
                " has already been registered. Not registering to new handler."
            ].join("");

            var handlerTypeError = [
                "Handler for command named ",
                name,
                " must be a function and not ",
                typeof handler,
                "."
            ].join("");

            var nameTypeError = [
                "Command names must be strings and not ",
                typeof name,
                "."
            ].join("");

            // Ensure only functions can be registered as handlers
            if (typeof handler !== 'function') {
                throw new Error(handlerTypeError);
            }

            // Ensure only strings can be registered as names
            if (typeof name !== 'string') {
                throw new Error(nameTypeError);
            }

            // Prevent double registration
            if (typeof this.registry[name] === 'function') {
                console.warn(alreadyRegisteredError);
                return;
            }

            this.registry[name] = handler;
        },


        // Request the bus handles the specified command
        dispatch: function(name, args) {
            var SUCCESS_SUFFIX = ".success";
            var FAILURE_SUFFIX = ".failure";

            var self = this;

            return new Promise(function(resolve, reject) {
                    try {
                        var response = self.registry[name].apply(self, args);
                        return resolve(response);
                    } catch (errorMessage) {
                        return reject(errorMessage);
                    }
                })
                .then(function success(response) {
                    // Publish success
                    self.publish(name + SUCCESS_SUFFIX, {
                        arguments: args,
                        response: response
                    }, self);

                    return response;
                })
                .catch(function failure(error) {
                    // Publish failure
                    self.publish(name + FAILURE_SUFFIX, {
                        arguments: args,
                        error: error
                    }, self);
                });
        }
    };
})();